import React from 'react';
import '../index.css'

function Footer() {
  return (
    <div className='footer'>
        <div className='footer-left'> FACEBOOK </div>
        <div className='footer-right'>
            <ul className='footerlist'>
                <li> Friends </li>
                <li> Group </li>
                <li> Market Place </li>
                <li> Watch </li>
                <li> Setting</li>
            </ul>
        </div>
    </div>
  )
}

export default Footer