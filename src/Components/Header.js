import React from 'react';
import MyImage from "../../src/images/icons8-facebook-480.png"
import Home from '../../src/images/icons8-home-384.png'
import video from '../../src/images/video.png'
import store from '../../src/images/shop.png'
import setting from '../../src/images/settings.png'
import matrix from '../../src/images/matrix.png'
import message from '../../src/images/message.png'
import notifiaction from '../../src/images/notify.png'
import '../index.css'
function Header() {
  return (
    <div className='header'>
        
        <div className='header-left'> 
        <img src={MyImage} className="logo fblogo"/>
        </div>
        <div className='header-center'>
        <img src={Home} className="logo"/>
        <img src={video} className="logo"/>
        <img src={store} className="logo"/>
        <img src={setting} className="logo"/>

        </div>
        <div className='header-right'>
        <img src={matrix} className="logo"/>
        <img src={message} className="logo"/>
        <img src={notifiaction} className="logo"/>
        </div>
    </div>
  )
}

export default Header