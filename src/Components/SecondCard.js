import React, { Component } from 'react'
import Like from '../images/like-svgrepo-com.svg'
import Comment from '../images/comment-svgrepo-com.svg'
import share from '../images/share-svgrepo-com.svg'
import MotherAndSon from '../images/motherAndSon.jpg'
import '../index.css'

class SecondContainer extends Component {
  state = { isLiked: false, Likes: 0, Share: 0 }
    likeMessage = () => {
        const { Likes } = this.state
        if (Likes === 1) {
            this.setState((prevState) =>
            ({
                Likes: prevState.Likes = 0
            })
            )
        } else {
            this.setState((prevState) =>
            ({
                Likes: prevState.Likes = 1
            })
            )
        }
    }
    ShareMessage = () => {
        const {Share} = this.state
        if(Share===1){
            this.setState((prevState) =>
            ({ Share: prevState.Share = 0 })
        )
        } else {
            this.setState((prevState) =>
            ({ Share: prevState.Share = 1 })
        )
        }
    }
  render() {
    const {Likes,Share}=this.state
    return (
    <div className='all-cards'>   
    <div className='profile-container'>
        <div className='name-container'> 
        <h4> Hina</h4>
        </div>
        <img src={MotherAndSon} className="main-images" alt='panda'/>
        <div className='card-detail'>
        <h5> #motherHood </h5>
        <p>Being a mom has made me so tired. And so happy.</p> 
        </div>
        <div className='reaction-container'>
        <div className='like-container'><button id='like' onClick={this.likeMessage}>  <img src={Like} className='reaction' alt='like'/></button> <p> {Likes}</p> </div>
         <div className='like-container'><button> <img src={Comment} className='reaction'/> </button> </div>
         <div className='like-container'><button id='like' onClick={this.ShareMessage}>  <img src={share} className='reaction'/> </button> <p> {Share}</p> </div>
        
        </div>
    </div>    
    </div>
    )
  }
}

export default SecondContainer