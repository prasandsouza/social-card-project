import React, { Component } from 'react'
import Like from '../images/like-svgrepo-com.svg'
import Comment from '../images/comment-svgrepo-com.svg'
import share from '../images/share-svgrepo-com.svg'
import PolarBear from '../images/polarbear.jpg'
import FirstCard from './FirstCard'
import SecondCard from './SecondCard'
import ThirdCard from './ThirdCard'
import FourthCard from './FourthCard'
import Fifthcard from './Fifthcard'

class MainContent extends Component {
  // state={isLiked:false, Likes: 0,Share:0}
  // likeMessage = ()=>{
  //    this.setState((prevState)=>
  //      ({Likes: prevState.Likes+1})
  //   )
  // }
  // ShareMessage =() =>{
  //   this.setState((prevState)=>
  //   ({Share : prevState.Share+1})
  //   )
  // }
  render() {
    // const {Likes,Share}=this.state
    return (
    <div className='all-cards'>   
    <FirstCard/> 
    <SecondCard/>
    <ThirdCard/>
    <FourthCard/>
    <Fifthcard/>  
    </div>
    )
  }
}

export default MainContent