import React, { Component } from 'react'
import Like from '../images/like-svgrepo-com.svg'
import Comment from '../images/comment-svgrepo-com.svg'
import share from '../images/share-svgrepo-com.svg'
import PolarBear from '../images/polarbear.jpg'
import '../index.css'

class Fifthcard extends Component {
  state = { isLiked: false, Likes: 0, Share: 0 }
    likeMessage = () => {
        const { Likes } = this.state
        if (Likes === 1) {
            this.setState((prevState) =>
            ({
                Likes: prevState.Likes = 0
            })
            )
        } else {
            this.setState((prevState) =>
            ({
                Likes: prevState.Likes = 1
            })
            )
        }
    }
    ShareMessage = () => {
        const {Share} = this.state
        if(Share===1){
            this.setState((prevState) =>
            ({ Share: prevState.Share = 0 })
        )
        } else {
            this.setState((prevState) =>
            ({ Share: prevState.Share = 1 })
        )
        }
    }
  render() {
    const {Likes,Share}=this.state
    return (
    <div className='all-cards'>   
       <div className='profile-container'>
        <div className='name-container'> 
        <h4> Nat Geo</h4>
        </div>
        <img src={PolarBear} className="main-images" alt='panda'/>
        <div className='card-detail'>
        <h5> #endangered </h5>
        <p>because of ongoing and potential loss of their sea ice habitat resulting from climate change–the primary threat to polar bears Arctic-wide–polar bears were listed as a threatened species in the US under the Endangered Species Act in May 2008.</p>
        </div>
        <div className='reaction-container'>
        <div className='like-container'> <button id='like' onClick={this.likeMessage}>  <img src={Like} className='reaction' alt='like'/></button> <p> {Likes}</p></div>
         <div className='like-container'> <button> <img src={Comment} className='reaction'/> </button> </div>
        <div className='like-container'> <button id='like' onClick={this.ShareMessage}>  <img src={share} className='reaction'/> </button> <p> {Share}</p></div>
        
        </div>
    </div>
    </div>
    )
  }
}

export default Fifthcard