import React, { Component } from 'react'
import Panda from "../images/panda.jpg"
import Like from '../images/like-svgrepo-com.svg'
import Comment from '../images/comment-svgrepo-com.svg'
import share from '../images/share-svgrepo-com.svg'
import '../index.css'

class FirstCard extends Component {
    state = { isLiked: false, Likes: 0, Share: 0 }
    likeMessage = () => {
        const { Likes } = this.state
        if (Likes === 1) {
            this.setState((prevState) =>
            ({
                Likes: prevState.Likes = 0
            })
            )
        } else {
            this.setState((prevState) =>
            ({
                Likes: prevState.Likes = 1
            })
            )
        }
    }
    ShareMessage = () => {
        const {Share} = this.state
        if(Share===1){
            this.setState((prevState) =>
            ({ Share: prevState.Share = 0 })
        )
        } else {
            this.setState((prevState) =>
            ({ Share: prevState.Share = 1 })
        )
        }
    }
    render() {
        const { Likes, Share } = this.state
        return (
            <div className='all-cards'>
                <div className='profile-container'>
                    <div className='name-container'>
                        <h4> Panda</h4>
                    </div>
                    <img src={Panda} className="main-images" alt='panda' />
                    <div className='card-detail'>
                        <h5> #Panda photo </h5>
                        <p>Giant pandas have a distinctive black and white coat, with black fur around their eyes and on their ears, muzzle, legs and shoulders</p>
                    </div>
                    <div className='reaction-container'>
                        <div className='like-container'><button id='like' onClick={this.likeMessage}>  <img src={Like} className='reaction' alt='like' /></button> <p> {Likes}</p> </div>
                        <div className='like-container'><button> <img src={Comment} className='reaction' /> </button> </div>
                        <div className='like-container'><button id='like' onClick={this.ShareMessage}>  <img src={share} className='reaction' /> </button> <p> {Share}</p></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FirstCard